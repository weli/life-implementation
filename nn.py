import torch
import torch.nn as nn
import numpy as np
import dataset

x, y = dataset.loadData('balance-scale')
y = np.mat(y).T
X = torch.tensor(x, dtype=torch.int) # 3 X 2 tensor
y = torch.tensor(y, dtype=torch.int)
xPredicted = torch.tensor(([5,2,4,1]), dtype=torch.int) # 1 X 2 tensor
#xPredicted = X

# scale units
X_max, _ = torch.max(X, 0)
xPredicted_max, _ = torch.max(xPredicted, 0)

X = torch.div(X, X_max)
xPredicted = torch.div(xPredicted, xPredicted_max)

class Neural_Network(nn.Module):
    def __init__(self, ):
        super(Neural_Network, self).__init__()
        # parameters
        # TODO: parameters can be parameterized instead of declaring them here
        self.inputSize = 4
        self.outputSize = 1
        self.hiddenSize = 3
        self.datasize = 1
        
        # weights
        self.W1 = torch.randn(self.inputSize, self.hiddenSize) # 4 X 3 tensor
        self.W2 = torch.randn(self.hiddenSize, self.outputSize) # 3 X 1 tensor
        self.B1 = torch.randn(self.datasize, self.hiddenSize)
        self.B2 = torch.randn(self.datasize, self.outputSize)

    def setParameters(self, input, output, hidden, size):
        self.inputSize = input
        self.outputSize = output
        self.hiddenSize = hidden
        self.datasize = size
        
        # weights
        self.W1 = torch.randn(self.inputSize, self.hiddenSize) # 4 X 3 tensor
        self.W2 = torch.randn(self.hiddenSize, self.outputSize) # 3 X 1 tensor
        self.B1 = torch.randn(self.datasize, self.hiddenSize)
        self.B2 = torch.randn(self.datasize, self.outputSize)
        
    def forward(self, X):
        self.z = torch.matmul(X, self.W1) # 625 X 3 ".dot" does not broadcast in PyTorch
        self.z2 = self.sigmoid(self.z + self.B1) # activation function
        self.z3 = torch.matmul(self.z2, self.W2)
        o = self.sigmoid(self.z3 + self.B2) # final activation function
        return o
        
    def sigmoid(self, s):
        return 1 / (1 + torch.exp(-s))
    
    def sigmoidPrime(self, s):
        # derivative of sigmoid
        return s * (1 - s)
    
    def backward(self, X, y, o):
        self.o_error = y - o # error in output
        self.o_delta = self.o_error * self.sigmoidPrime(o) # derivative of sig to error
        self.z2_error = torch.matmul(self.o_delta, torch.t(self.W2)) + self.B2
        self.z2_delta = self.z2_error * self.sigmoidPrime(self.z2)
        self.B1 += self.z2_delta
        self.B2 += self.o_delta
        self.W1 += torch.matmul(torch.t(X), self.z2_delta)
        self.W2 += torch.matmul(torch.t(self.z2), self.o_delta)
        
    def train(self, X, y):
        # forward + backward pass for training
        o = self.forward(X)
        self.backward(X, y, o)
        
    def saveWeights(self, model, name):
        # we will use the PyTorch internal storage functions
        torch.save(model, name)
        # you can reload model with all the weights and so forth with:
        # torch.load("NN")
        #return self.W1, self.W2, self.B1, self.B2
        
    def predict(self,xPredicted):
        if(np.shape(np.shape(xPredicted))[0]==1):
            self.setParameters(4,1,3,1)
        else:
            self.setParameters(4,1,3,np.shape(xPredicted)[0])
        print ("Predicted data based on trained weights: ")
        print ("Input (scaled): \n" + str(xPredicted))
        print ("Output: \n" + str(self.forward(xPredicted)))
        print(self.W1)
        print(self.W2)

    #def saveWeightsAndBiais(self):
     #   return self.

cp = 10

NN1 = Neural_Network()
NN1.setParameters(4,1,3,625)
NN2 = Neural_Network()
NN2.setParameters(4,1,3,625)

for i in range(50):  # trains the NN 1,000 times
    print ("#" + str(i) + " Loss: " + str(torch.mean((y - NN1(X))**2).detach().item()))  # mean sum squared loss
    NN1.train(X, y)
for i in range(50):  # trains the NN 1,000 times
    print ("#" + str(i) + " Loss: " + str(torch.mean((y - NN2(X))**2).detach().item()))  # mean sum squared loss
    NN2.train(X, y)
NN1.saveWeights(NN1, "NN1")
NN2.saveWeights(NN2, "NN2")

NN1.predict(xPredicted)
NN2.predict(xPredicted)
